import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.lang.*;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import javax.imageio.ImageIO;
import javax.swing.table.*;
import java.awt.image.BufferedImage;




public class  MyFrame extends Frame{
	
    String msg = "";
    
    String m1 = "";
    String m2="";
	
	/*
    @Override
         public void paint(Graphics g){
		g.setColor(Color.BLACK);
                g.drawString(msg,100,100);
                }
	*/
	
	public void  superr(ButtonSensor b)
        {
		
		setTitle("Ware House Management");
		
		
		Button start=new Button("Start");
		
		Button exit=new Button("Exit");
		
	
                msg="Welcome to warehouse";
                
                 BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                
                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
                
		
                
                
               Label label1 = new Label("Welcome TO ABN Ware house");
		backGround.add(label1);
		label1.setBounds(200 , 50, 220, 20);
		label1.setFont(new Font("Serif", Font.BOLD, 15));
		label1.setForeground(Color.BLACK);
                
		
		//start
		start.setBounds(250, 150, 90, 30);
		backGround.add(start); 
		//
		
		
		
		 //Exit
		exit.setBounds(250, 220,  90, 30);
		backGround.add(exit); 
		//
		
		
		start.addActionListener(b);
		//worker.addActionListener(b);
		exit.addActionListener(b);
                
              
                
               setSize(600,400);
               setVisible(true);
		
	}
       
   
	
	public void Login(ButtonSensor b)
        {
		
			
		
		this.setTitle("Login");
		
		
		TextField user=new TextField(30);
		JPasswordField pass = new JPasswordField(30);
		
		Button logina=new Button("Login");
		Button forgota=new Button("Forgot Password ?");
		
		
		
		BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
		
		setSize(600,400);
               setVisible(true);
		
		
		Button back=new Button("Back");
		back.setBounds(50, 50, 50, 25);
		backGround.add(back);
		back.addActionListener(b);
		//ButtonSensor bs2=new ButtonSensor();
		
		
		
		
		
		
		
		Label label1 = new Label("User Name");
		backGround.add(label1);
		label1.setBounds(280 , 90, 100, 25);
		label1.setFont(new Font("Serif", Font.BOLD, 15));
		label1.setForeground(Color.BLACK);
		
		
		
		//Username  Text Field
		user.setBounds(400, 90, 150, 25);
		backGround.add(user);  
		//
		
                
		Label label2 = new Label("Password");
		backGround.add(label2);
		label2.setBounds(280,140, 100, 25);
		label2.setFont(new Font("Serif", Font.BOLD, 15));
		label2.setForeground(Color.BLACK);
		
		
		
		//Password  Text Field
		pass.setBounds(400, 140, 150, 25);
		backGround.add(pass);  
		//
		
		//Login button
		logina.setBounds(400, 190, 90, 25);
		backGround.add(logina); 
		//
		
		//Forgot button
		forgota.setBounds(400,240,  120, 25);
		backGround.add(forgota);  
		//
		
		b.setTextFields_login(user,pass);
		
		logina.addActionListener(b);
		forgota.addActionListener(b);
		
		
				
		setSize(600,400);
                setVisible(true);
		
		
		
	}
	
	
	public void Admin_verified (ButtonSensor b)
        {
		
				
		
		this.setTitle("Admin ");
		
		
		Button Warehouse=new Button("Warehouse");
		Button SellReport=new Button("Sell Report");
		Button ChangePass=new Button("Change Password");
		
		
		
		
		
		
		BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
		
		
		
		Label label = new Label("Welcome to Admin panel");
		backGround.add(label);
		label.setBounds(200,100, 230, 25);
		label.setFont(new Font("Serif", Font.BOLD, 20));
		label.setForeground(Color.BLACK);
		
		//Warehouse button
		Warehouse.setBounds(100, 170, 120, 25);
		backGround.add(Warehouse); 
		//
		
		//Sell Report button
		SellReport.setBounds(250,170,  120, 25);
		backGround.add(SellReport);  
		//
		
		//Change Password Report button
		ChangePass.setBounds(400,170,  120, 25);
		backGround.add(ChangePass);  
		//
		
		Warehouse.addActionListener(b);
		SellReport.addActionListener(b);
		ChangePass.addActionListener(b);
		
				
		setSize(600,400);
                setVisible(true);
		
		
		
	}
	
	
        
	
	public void WareHouse (ButtonSensor b)
        {
		
				
		
		this.setTitle("Warehouse");
		
		
		
		
		Button prList=new Button("Product List");
		Button addPrdt=new Button("Add Product");
		
		Button back=new Button("Back");
		
		
		
		
		BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
		
		
		
		
		
		back.setBounds(50, 50, 50, 25);
		backGround.add(back);
		back.addActionListener(b);
		
		
		
		Label label = new Label("Choose any one");
		backGround.add(label);
		label.setBounds(100,100, 300, 25);
		label.setFont(new Font("Serif", Font.BOLD, 20));
		label.setForeground(Color.GRAY);
		
		//Warehouse button
		prList.setBounds(100, 170, 120, 25);
		backGround.add(prList); 
		//
		
		//Sell Report button
		addPrdt.setBounds(250,170,  120, 25);
		backGround.add(addPrdt);  
		//
		
		
		prList.addActionListener(b);
		addPrdt.addActionListener(b);
		
		
				
		setSize(600,400);
                setVisible(true);
		
		
	}
	
	
	
	public void AddProduct (ButtonSensor b)
        {
		
				
		
		this.setTitle("Add Product");
		
		
		
		
		BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
		
		
		
		
		Label label6 = new Label("Product ID");
		backGround.add(label6);
		label6.setBounds(200,50, 150, 25);
		label6.setFont(new Font("Serif", Font.BOLD, 16));
		label6.setForeground(Color.BLACK);
		
		TextField pID = new TextField();
		
		pID.setBounds(350,50,170,25);
		backGround.add(pID);
		
		
		
		
		Label label1 = new Label("Product Name");
		backGround.add(label1);
		label1.setBounds(200,90, 150, 25);
		label1.setFont(new Font("Serif", Font.BOLD, 16));
		label1.setForeground(Color.BLACK);
		
		TextField pName = new TextField();
		
		pName.setBounds(350,90,170,25);
		backGround.add(pName);
		
		
		
		
		
		
		//String[] q = {"Electronics" , "CLoth" , "Juelary" };
		
		Label label2 = new Label("Product Catagory");
		backGround.add(label2);
		label2.setBounds(200,130, 150, 25);
		label2.setFont(new Font("Serif", Font.BOLD, 16));
		label2.setForeground(Color.BLACK);
		
		/*
		 JComboBox cat = new JComboBox(q);
		cat.setBounds(350,50,  170, 25);
		add(cat);
		*/
		
		TextField pCat = new TextField();
		pCat.setBounds(350,130,170,25);
		backGround.add(pCat);
		
		
		
		Label label3 = new Label("Product Quantity");
		backGround.add(label3);
		label3.setBounds(200,170, 150, 25);
		label3.setFont(new Font("Serif", Font.BOLD, 16));
		label3.setForeground(Color.BLACK);
		
		TextField pQuan = new TextField();
		
		pQuan.setBounds(350,170,170,25);
		backGround.add(pQuan);
		
		
		
		Label label4 = new Label("Buying Price");
		backGround.add(label4);
		label4.setBounds(200,210, 150, 25);
		label4.setFont(new Font("Serif", Font.BOLD, 16));
		label4.setForeground(Color.BLACK);
		
		TextField buyPrice = new TextField();
		
		buyPrice.setBounds(350,210,170,25);
		backGround.add(buyPrice);
		
		
		
		Label label5 = new Label("Selling Price");
		backGround.add(label5);
		label5.setBounds(200,250, 150, 25);
		label5.setFont(new Font("Serif", Font.BOLD, 16));
		label5.setForeground(Color.BLACK);
		
		TextField sellPrice = new TextField();
		
		sellPrice.setBounds(350,250,170,25);
		backGround.add(sellPrice);
		
		
		
		Button save = new Button("Save");
		save.setBounds(450,300, 50, 25);
		backGround.add(save); 
		
		
		Button back=new Button("Back");
		back.setBounds(50, 50, 50, 25);
		backGround.add(back);
		back.addActionListener(b);
		
		
		b.setTextFields_addPro(pID,pName, pCat ,   pQuan, buyPrice, sellPrice);
		
		
		save.addActionListener(b);
		
		
				
		setSize(600,400);
                setVisible(true);
		
		
		
	}
	
	
     
	
	
	public void worker_verified (ButtonSensor b)
        {
		
				
		
		this.setTitle("Worker");
		
		
		
		
		
		
		
		BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
		
		
		Button newSell=new Button("Add new sell");
		Button sellRpt=new Button("Sell Report");
		
		
		
		
		Label label = new Label("Welcome to sells panel");
		backGround.add(label);
		label.setBounds(200,100, 200, 25);
		label.setFont(new Font("Serif", Font.BOLD, 20));
		label.setForeground(Color.BLACK);
		
		//newSell button
		newSell.setBounds(150, 170, 120, 25);
		backGround.add(newSell); 
		//
		
		//Sell Report button
		sellRpt.setBounds(300,170,  120, 25);
		backGround.add(sellRpt);  
		//
		
		
		
		newSell.addActionListener(b);
		sellRpt.addActionListener(b);
		
		
				
		setSize(600,400);
                setVisible(true);
		
		
		
	}
	
	
	
	
	public void addNewSell(ButtonSensor b)
        {
		
		
		this.setTitle("Add new Sell");
		
		
		BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
		
		
		
		
		
		
		Label label1 = new Label("Product ID");
		backGround.add(label1);
		label1.setBounds(30,100, 100, 25);
		label1.setFont(new Font("Serif", Font.BOLD, 16));
		label1.setForeground(Color.BLACK);
		
		TextField pID = new TextField();
		
		pID.setBounds(170,100,170,25);
		backGround.add(pID);
		
		
		
		
		
		
		
		
		Label label2 = new Label("Product Quantity");
		backGround.add(label2);
		label2.setBounds(30,150, 130, 25);
		label2.setFont(new Font("Serif", Font.BOLD, 16));
		label2.setForeground(Color.BLACK);
		
		
		
		TextField pQn = new TextField();
		pQn.setBounds(170,150,170,25);
		backGround.add(pQn);
		
		
		
		Button OK = new Button("OK");
		OK.setBounds(220, 200, 50, 25);
		backGround.add(OK); 
		
		TextField sPr = new TextField();
		sPr.setBounds(300,230,170,80);
		backGround.add(sPr);
		
		Button save = new Button("Save");
		save.setBounds(350, 320, 50, 25);
		backGround.add(save); 
		
		
		Button back=new Button("Back");
		back.setBounds(40, 40, 50, 25);
		backGround.add(back);
		back.addActionListener(b);
		
		
		b.setTextFields_addNeSell(pID,   pQn , sPr);
		
		
		save.addActionListener(b);
		OK.addActionListener(b);
		
				
		setSize(600,400);
                setVisible(true);
	}
        
        
        
        
        
        public void SellReport  (ButtonSensor b)
        {
		
				
		
		this.setTitle("Sell Report");
		
		
		Button today=new Button("Todays Report");
		Button all=new Button("All Report");
		
		
		BufferedImage img = null;
                try {
                    img = ImageIO.read(new File("F:\\img.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Image backImg = img.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
                ImageIcon backImageIcon = new ImageIcon(backImg);   //creates background image icon

                setLayout(new BorderLayout());

                JLabel backGround=new JLabel(backImageIcon);
                add(backGround);
	
		
		
		Button OK=new Button("OK");
		
		Button back=new Button("Back");
		back.setBounds(50, 50, 50, 25);
		backGround.add(back);
		back.addActionListener(b);
                
                
                
                
                Label label = new Label("Day");
		backGround.add(label);
		label.setBounds(102,100, 45, 25);
		label.setFont(new Font("Serif", Font.BOLD, 15));
		label.setForeground(Color.BLACK);
                
                TextField day1 = new TextField();
		day1.setBounds(100,130,50,25);
		backGround.add(day1);
                
                Label labe2 = new Label("Month");
		backGround.add(labe2);
		labe2.setBounds(157,100, 45, 25);
		labe2.setFont(new Font("Serif", Font.BOLD, 15));
		labe2.setForeground(Color.BLACK);
                
                TextField Month1 = new TextField();
		Month1.setBounds(155,130,50,25);
		backGround.add(Month1);
                
                
                 Label labe3 = new Label("Year");
		backGround.add(labe3);
		labe3.setBounds(212,100, 45, 25);
		labe3.setFont(new Font("Serif", Font.BOLD, 15));
		labe3.setForeground(Color.BLACK);
                
                TextField Year1 = new TextField();
		Year1.setBounds(210,130,50,25);
		backGround.add(Year1);
                
                
                Label to = new Label("TO");
		backGround.add(to);
		to.setBounds(275,130, 30, 25);
		to.setFont(new Font("Serif", Font.BOLD, 15));
		to.setForeground(Color.BLACK);
                
                
                Label labe4 = new Label("Day");
		backGround.add(labe4);
		labe4.setBounds(324,100, 45 , 25);
		labe4.setFont(new Font("Serif", Font.BOLD, 15));
		labe4.setForeground(Color.BLACK);
                
                TextField day2 = new TextField();
		day2.setBounds(322,130,50,25);
		backGround.add(day2);
                
                Label labe5 = new Label("Month");
		backGround.add(labe5);
		labe5.setBounds(379,100, 45, 25);
		labe5.setFont(new Font("Serif", Font.BOLD, 15));
		labe5.setForeground(Color.BLACK);
                
                TextField Month2 = new TextField();
		Month2.setBounds(377,130,50,25);
		backGround.add(Month2);
                
                
                 Label labe6 = new Label("Year");
		backGround.add(labe6);
		labe6.setBounds(434,100, 45, 25);
		labe6.setFont(new Font("Serif", Font.BOLD, 15));
		labe6.setForeground(Color.BLACK);
                
                TextField Year2 = new TextField();
		Year2.setBounds(432,130,50,25);
		backGround.add(Year2);
                
                
		
		
		//OK button
		OK.setBounds(275, 200, 50, 25);
		backGround.add(OK); 
		//
		
		b.setTextFields_sellReport(day1, day2, Month1 , Month2 ,Year1 , Year2);
		
		
		
		OK.addActionListener(b);
		day1.addActionListener(b);
                day2.addActionListener(b);
                Month1.addActionListener(b);
		Month2.addActionListener(b);
                Year1.addActionListener(b);
		Year2.addActionListener(b);
		
		
				
		setSize(600,400);
                setVisible(true);
		
		
		
	}
        
        
        public void ProductList(ButtonSensor b)
        {
            
            
			DefaultTableModel model = new DefaultTableModel();
                        
                        Button cl=new Button("Close");
                        cl.setBounds(400, 600, 100, 30);
                        
			String d;
                        DateFormat df;
                        df = new SimpleDateFormat("yyyy-MM-dd");

                        Date dateob = new Date();
                        d=df.format(dateob);

                        DataAccess da = new DataAccess();
                        ResultSet rs=null;
                        String columnNames[]= { "PRODUCT_ID", "PRODUCT_NAME", "PRODUCT_CATAGORY" , "QUANTITY" , "BUYING_PRICE" , "SELLING_PRICE" , "LAST_UPDATE_DATE"};

                       for(int i=0;i<columnNames.length;i++){
			model.addColumn(columnNames[i]);      //ADDS COLUMNS 
                        }
                       
                 JTable table = new JTable(model);
		model = (DefaultTableModel) table.getModel();
                
                        String p = "Select * from addproduct";
			
                        String rowData[]=new String[7];
                             
			rs=da.getData(p);
			try{
                            
			while(rs.next()){
                            
                            System.out.println("*****");
                            
			   rowData[0] =Integer.toString(rs.getInt("PRODUCT_ID"));
                           
                           rowData[1] = rs.getString("PRODUCT_NAME");
                           
                           rowData[2] = rs.getString("PRODUCT_CATAGORY");
                           
                           rowData[3] = Integer.toString(rs.getInt("QUANTITY"));
                           
                           rowData[4] = Integer.toString(rs.getInt("BUYING_PRICE"));
                           
                           rowData[5] = Integer.toString(rs.getInt("SELLING_PRICE"));
                           
                           String t = "";
                           t=t+ rs.getDate("LAST_UPDATE_DATE");
                           rowData[6] = t;
                         
                            model.addRow(rowData);      
                         
			}
			 table.setModel(model);
                
                        }
                        catch(Exception e){System.out.println("#####");}
                
                        JScrollPane jsp=new JScrollPane(table);
                 
                         add(jsp);
                        add(cl);

                        cl.addActionListener(b);

                        WindowSensor ws=new WindowSensor();
                        this.addWindowListener(ws);
                        setSize(800,600);
                        
                         table.setPreferredScrollableViewportSize(new Dimension(this.getWidth()-10,400));

                        setLayout(new FlowLayout());
                        
            
            
        }        
        
        
        
         
                        
    
        
    public void SelLReportShow(ButtonSensor b , String a, String c)
    {
        DefaultTableModel model = new DefaultTableModel();
        
        m1=a;
        m2=c;
			
        System.out.println(m1 + " ***  "+m2);
        
			
                        Button cl=new Button("Close");
                        cl.setBounds(400, 600, 100, 30);
                        
			String d;
                        DateFormat df;
                        df = new SimpleDateFormat("yyyy-MM-dd");

                        Date dateob = new Date();
                        d=df.format(dateob);

                        DataAccess da = new DataAccess();
                        ResultSet rs=null;
                        String columnNames[]= { "PRODUCT_ID", "PRODUCT_NAME", "PRODUCT_CATAGORY" , "QUANTITY" ,  "SELLING_PRICE" , "SELL_AMOUNT", "SELLING_DATE"};

                       for(int i=0;i<columnNames.length;i++){
			model.addColumn(columnNames[i]);      //ADDS COLUMNS 
                        }
                       
                 JTable table = new JTable(model);
		model = (DefaultTableModel) table.getModel();
                
                         String p = "Select * from addsell where DATE  BETWEEN '"+m1+"' AND '"+m2+"'";
			
                        String rowData[]=new String[7];
                             
			rs=da.getData(p);
			try{
                            
			while(rs.next()){
                            
                            System.out.println("*****");
                            
			   rowData[0] =Integer.toString(rs.getInt("PRODUCT_ID"));
                           
                           rowData[1] = rs.getString("PRODUCT_NAME");
                           
                           rowData[2] = rs.getString("PRODUCT_CATAGORY");
                           
                           rowData[3] = Integer.toString(rs.getInt("QUANTITY"));
                           
                           rowData[4] = Integer.toString(rs.getInt("SELLING_PRICE"));
                           
                           rowData[5] = Integer.toString(rs.getInt("SELL_AMOUNT"));
                           
                           String t = "";
                           t=t+ rs.getDate("DATE");
                           rowData[6] = t;
                         
                            model.addRow(rowData);      
                         
			}
			 table.setModel(model);
                
                        }
                        catch(Exception e){System.out.println("#####");}
                
                        JScrollPane jsp=new JScrollPane(table);
                 
                         add(jsp);
                        add(cl);

                        cl.addActionListener(b);

                        WindowSensor ws=new WindowSensor();
                        this.addWindowListener(ws);
                        setSize(800,600);
                        
                         table.setPreferredScrollableViewportSize(new Dimension(this.getWidth()-10,400));

                        setLayout(new FlowLayout());                
        
        
                        
                        
                    }
        
        
	
	
}
