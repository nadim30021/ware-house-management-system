-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2015 at 03:49 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `addproduct`
--

CREATE TABLE IF NOT EXISTS `addproduct` (
  `PRODUCT_ID` int(11) NOT NULL,
  `PRODUCT_NAME` varchar(20) NOT NULL,
  `PRODUCT_CATAGORY` varchar(20) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `BUYING_PRICE` int(11) NOT NULL,
  `SELLING_PRICE` int(11) NOT NULL,
  `LAST_UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`PRODUCT_ID`),
  UNIQUE KEY `PRODUCT_ID` (`PRODUCT_ID`),
  UNIQUE KEY `PRODUCT_ID_2` (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addproduct`
--

INSERT INTO `addproduct` (`PRODUCT_ID`, `PRODUCT_NAME`, `PRODUCT_CATAGORY`, `QUANTITY`, `BUYING_PRICE`, `SELLING_PRICE`, `LAST_UPDATE_DATE`) VALUES
(1, 'Casio', 'WristWatch', 16, 600, 700, '2015-12-20'),
(2, 'Gents Shirt', 'Shirt', 32, 700, 800, '2015-12-20'),
(3, 'Nike', 'Shoe', 14, 2500, 2700, '2015-12-20'),
(4, 'Football', 'Sports', 12, 1600, 1800, '2015-12-20'),
(5, 'Omega', 'WristWatch', 30, 1000, 1200, '2015-12-20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
