-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2015 at 03:49 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `addsell`
--

CREATE TABLE IF NOT EXISTS `addsell` (
  `PRODUCT_ID` int(11) NOT NULL,
  `PRODUCT_NAME` varchar(20) NOT NULL,
  `PRODUCT_CATAGORY` varchar(20) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `SELLING_PRICE` int(11) NOT NULL,
  `SELL_AMOUNT` int(11) NOT NULL,
  `DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addsell`
--

INSERT INTO `addsell` (`PRODUCT_ID`, `PRODUCT_NAME`, `PRODUCT_CATAGORY`, `QUANTITY`, `SELLING_PRICE`, `SELL_AMOUNT`, `DATE`) VALUES
(1, 'Casio', 'WristWatch', 2, 700, 1400, '2015-12-17'),
(2, 'Gents Shirt', 'Shirt', 1, 800, 800, '2015-12-17'),
(3, 'Nike', 'Shoe', 2, 2700, 5400, '2015-12-17'),
(4, 'Football', 'Sports', 1, 1800, 1800, '2015-12-17'),
(1, 'Casio', 'WristWatch', 2, 700, 1400, '2015-12-18'),
(3, 'Nike', 'Shoe', 1, 2700, 2700, '2015-12-18'),
(4, 'Football', 'Sports', 1, 1800, 1800, '2015-12-18'),
(2, 'Gents Shirt', 'Shirt', 2, 800, 1600, '2015-12-18'),
(1, 'Casio', 'WristWatch', 1, 700, 700, '2015-12-18'),
(1, 'Casio', 'WristWatch', 1, 700, 700, '2015-12-20'),
(1, 'Casio', 'WristWatch', 1, 700, 700, '2015-12-20'),
(2, 'Gents Shirt', 'Shirt', 1, 800, 800, '2015-12-20'),
(3, 'Nike', 'Shoe', 1, 2700, 2700, '2015-12-20'),
(4, 'Football', 'Sports', 1, 1800, 1800, '2015-12-20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
